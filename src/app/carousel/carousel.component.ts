import { Component } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent {
  images = [{
    name: 'Slide 1',
    source: '../../assets/slider1.png'
  } , {
    name: 'Slide 2',
    source: '../../assets/slider2.png'
  } , {
    name: 'Slide 3',
    source: '../../assets/slider3.png'
  }];
}
