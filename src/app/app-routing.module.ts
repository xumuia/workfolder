import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './mainwindow/shortInfo/main.component';
import {FullinfoComponent} from './mainwindow/fullItemInfo/fullinfo.component';
import {CartComponent} from './mainwindow/cart/cart.component';
import {LoginComponent} from './login/login.component';

const routes: Routes = [
  {path: '', component: MainComponent},
  {path: 'item/:uid', component: FullinfoComponent},
  {path: 'cart', component: CartComponent},
  {path: 'login', component: LoginComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
