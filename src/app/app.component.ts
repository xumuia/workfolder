import { Component, OnInit } from '@angular/core';

import {CartService} from './mainwindow/api/cart.service';
import {Observable} from 'rxjs';
import {Item} from './mainwindow/api/item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public shoppingCartItems$: Observable<Item[]>;
 constructor(private cartService: CartService) {
   this.shoppingCartItems$ = this
     .cartService
     .getItems();

   this.shoppingCartItems$.subscribe(_ => _);
 }}
