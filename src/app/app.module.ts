import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {HttpClientModule} from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CarouselComponent} from './carousel/carousel.component';
import {MainComponent} from './mainwindow/shortInfo/main.component';
import {MatButtonModule} from '@angular/material';
import {FullinfoComponent} from './mainwindow/fullItemInfo/fullinfo.component';
import {ApiService} from './mainwindow/api/api.service';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule} from '@angular/forms';
import {CartService} from './mainwindow/api/cart.service';
import {CartComponent} from './mainwindow/cart/cart.component';
import {MatRadioModule} from '@angular/material/radio';
import {CartSizesPipe} from './mainwindow/cart/cartSizes.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import {LoginComponent} from './login/login.component';
import {AuthService} from './mainwindow/api/auth.service';


@NgModule({
  declarations: [
    AppComponent, CarouselComponent, MainComponent, FullinfoComponent, CartComponent, CartSizesPipe, LoginComponent
  ],
  imports: [NgbModule.forRoot(), BrowserModule, HttpClientModule, AppRoutingModule,
    BrowserAnimationsModule, MatButtonModule, MatIconModule, MatSelectModule, FormsModule, MatRadioModule, ReactiveFormsModule],
  providers: [ ApiService, CartService, AuthService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
