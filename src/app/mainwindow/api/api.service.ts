import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Item} from './item';


import {Observable} from 'rxjs';


@Injectable()
export class ApiService {
  constructor( private http: HttpClient) {}
  private data: Item[] = [];
  serverData: JSON;


  public addItem(url: string, description: string, price: number, imgUrl: string) {
     this.http.post(url, {
      description: description,
      price: price,
      imgUrl: imgUrl
    })
      .subscribe(
        res => {
          console.log(res);
        },
        error => {
          console.log('ERROR ERROR! PLEASE, ATTENTION!');
        }
      );
  }

  public getItems(): Observable<Item[]> {
      return this.http.get<Item[]>('http://127.0.0.1:5000/allitems');
  }
  public getItemById(id: number): Observable<Item> {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.get<Item>('http://127.0.0.1:5000/item/' + id, {headers: headers});
  }
}
