export class Item  {
  full_description: string;
  imgUrl: string;
  l: number;
  m: number;
  price: number;
  s: number;
  short_description: string;
  uid: number;
  xl: number;
  xs: number;
  xxl: number;
  brand: string;
  type: string;
  category: string;
  sex: string;
  discount: number;
  constructor (full_description: string, imgUrl: string, l: number, m: number, price: number, s: number,
               short_description: string, uid: number, xl: number, xs: number, xxl: number, brand: string, type: string,
               category: string, sex: string, discount: number) {}
}

