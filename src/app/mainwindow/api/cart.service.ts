import {Injectable} from '@angular/core';
import {Item} from './item';
import {BehaviorSubject, Observable, Subject, Subscriber} from 'rxjs';
import {of} from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable()
export class CartService {
  private itemsInCartSubject: BehaviorSubject<Item[]> = new BehaviorSubject([]);
  private itemsInCart: Item[] = [];

  constructor() {
    this.itemsInCartSubject.subscribe(_ => this.itemsInCart = _);
  }

  public addToCart(item: Item) {
    this.itemsInCartSubject.next([...this.itemsInCart, item]);
    console.log(item);
  }

  public getItems(): Observable<Item[]> {
    return this.itemsInCartSubject;
  }
  public getTotalAmount(): Observable<number> {
    return this.itemsInCartSubject.pipe(map((items: Item[]) => {
      return items.reduce((prev, curr: Item) => {
        return prev + (curr.price * ((100 - curr.discount) / 100));
      }, 0);
    }));
  }
}
