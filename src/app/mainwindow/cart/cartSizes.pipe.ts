import { Pipe, PipeTransform } from '@angular/core';

import {Item} from '../api/item';
import {Size} from './cart.component';

export interface Size {
  value: string;
  viewValue: string;
}

@Pipe({ name: 'filterSize' })
export class CartSizesPipe implements PipeTransform {
  index: number;
  transform(allSizes: Size[], curItem: Item): any[] {
     console.log('Im in pipe');
     console.log(curItem);
    if (curItem.xs === 0) {
      allSizes = allSizes.filter(
        size => size.value !== 'xs');
      console.log('xs');
    }
    if (curItem.s === 0) {
      allSizes = allSizes.filter(
        size => size.value !== 's');
    }
    if (curItem.m === 0) {
      allSizes = allSizes.filter(
        size => size.value !== 'm');
    }
    if (curItem.l === 0) {
      allSizes = allSizes.filter(
        size => size.value !== 'l');
    }
    if (curItem.xl === 0) {
      allSizes = allSizes.filter(
        size => size.value !== 'xl');
    }
    if (curItem.xxl === 0) {
      allSizes = allSizes.filter(
      size => size.value !== 'xxl');
     }
    return allSizes;
  }
}
