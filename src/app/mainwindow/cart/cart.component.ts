import { Component, OnInit} from '@angular/core';
import {Item} from '../api/item';
import {CartService} from '../api/cart.service';
import {of} from 'rxjs';
import {Observable} from 'rxjs';

export interface Size {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  public shoppingCartItems$: Observable<Item[]> = of([]);
  public shoppingCartItems: Item[] = [];
  sizeList: Size[] = [
    {value: 'xs', viewValue: 'XS'},
    {value: 's', viewValue: 'S'},
    {value: 'm', viewValue: 'M'},
    {value: 'l', viewValue: 'L'},
    {value: 'xl', viewValue: 'XL'},
    {value: 'xxl', viewValue: 'XXL'}
    ];
  total: number;
  constructor (private cartService: CartService) {
    this.shoppingCartItems$ = this
      .cartService
      .getItems();

    this.shoppingCartItems$.subscribe(_ => this.shoppingCartItems = _);
  }
  ngOnInit() {
    console.log(this.shoppingCartItems);
  }
  removeItem(ind: number) {
  this.shoppingCartItems.splice(ind, 1);
  this.total = 0;
  }
   public getTotal() {
     return this.cartService.getTotalAmount();
  }
}
