import { Component, OnInit } from '@angular/core';
import {Item} from '../api/item';
import {ApiService} from '../api/api.service';
import { Pipe, PipeTransform } from '@angular/core';

export interface Type {
  value: string;
  viewValue: string;
}
export interface Brand {
  value: string;
  viewValue: string;
}

export interface Size {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  constructor(private apiService: ApiService) {}
  typeList: Type[] = [
    {value: 'Clothing', viewValue: 'Clothing'},
    {value: 'Shoes', viewValue: 'Shoes'},
    {value: 'Hats', viewValue: 'Hats'},
    {value: '', viewValue: 'All'}
  ];
  brandList: Brand[] = [
    {value: 'XStyle', viewValue: 'XStyle'},
    {value: 'Grew & Brew', viewValue: 'Grew & Brew'},
    {value: 'Dr.Martens', viewValue: 'Dr.Martens'},
    {value: 'Crosby', viewValue: 'Crosby'},
    {value: 'Fete', viewValue: 'Fete'},
    {value: 'Pull&Bear', viewValue: 'Pull&Bear'},
    {value: '', viewValue: 'All'}
  ];

  sizeList: Brand[] = [
    {value: 'xs', viewValue: 'XS'},
    {value: 's', viewValue: 'S'},
    {value: 'm', viewValue: 'M'},
    {value: 'l', viewValue: 'L'},
    {value: 'xl', viewValue: 'XL'},
    {value: 'xxl', viewValue: 'XXL'},
    {value: '', viewValue: 'All'}
  ];
  private url: 'http://127.0.0.1:5000/items'
  items: Item[];
  keyValue: string;

  filtredItems: Item[];
  selectType: string;
  selectBrand: string;
  selectSize: string;
  ngOnInit() {
            this.apiService.getItems().subscribe(value => {
            this.items = value;
            this.filtredItems = this.items;
            console.log(this.items);
            }
      );
  }
  filter() {
   this.filtredItems = this.items;
   if (this.selectType !== '' && this.selectType !== undefined) {
     this.filtredItems = this.filtredItems.filter((item: Item) => item.type === this.selectType);
   }
   if (this.selectBrand !== '' && this.selectBrand !== undefined) {
     this.filtredItems = this.filtredItems.filter((item: Item) => item.brand === this.selectBrand);
   }
    if (this.selectSize !== '' && this.selectSize !== undefined) {
     if (this.selectSize === 'xs') {
       this.filtredItems = this.filtredItems.filter((item: Item) => item.xs > 0);
     }
      if (this.selectSize === 's') {
        this.filtredItems = this.filtredItems.filter((item: Item) => item.s > 0);
      }
      if (this.selectSize === 'm') {
        this.filtredItems = this.filtredItems.filter((item: Item) => item.m > 0);
      }
      if (this.selectSize === 'l') {
        this.filtredItems = this.filtredItems.filter((item: Item) => item.l > 0);
      }
      if (this.selectSize === 'xl') {
        this.filtredItems = this.filtredItems.filter((item: Item) => item.xl > 0);
      }
      if (this.selectSize === 'xxl') {
        this.filtredItems = this.filtredItems.filter((item: Item) => item.xxl > 0);
      }
    }


   console.log(this.filtredItems);
  }
  }
