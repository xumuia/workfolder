import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import {Item} from '../api/item';
import {ApiService} from '../api/api.service';
import {ActivatedRoute} from '@angular/router';

import {CartService} from '../api/cart.service';

@Component({
  selector: 'app-fullinfo',
  templateUrl: './fullinfo.component.html',
  styleUrls: ['./fullinfo.component.css']
})
export class FullinfoComponent implements OnInit {
  @Output() changed: EventEmitter<any> = new EventEmitter();
  constructor(private apiService: ApiService, private route: ActivatedRoute, private cartService: CartService) {
 }
  soloItem: Item;
  counter: number;
  ngOnInit() {

    this.apiService.getItemById(this.route.snapshot.params['uid']).subscribe(value => {
             this.soloItem = value;
             console.log(value);
             this.counter = localStorage.length;
           });
   }


   addToCart(item: Item) {
    this.cartService.addToCart(item);
    console.log(item);
   }
}
