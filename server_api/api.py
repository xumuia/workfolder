import flask
import json
from bson import ObjectId
from flask import request, jsonify
from flask_pymongo import PyMongo

app = flask.Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/api" #api/items
mongo = PyMongo(app)
app.config["DEBUG"] = True

items = []
class User(object):
  def __init__(self, username, password):
    self.username = username
    self.password = password

class Item(object):

  def __init__(self, uid, short_description, full_description, xs, s, m, l, xl, xxl, price, imgUrl, sex, brand, type,
               category, discount):
    self.uid = uid
    self.short_description = short_description
    self.full_description = full_description
    self.xs = xs
    self.s = s
    self.m = m
    self.l = l
    self.xl = xl
    self.xxl = xxl
    self.price = price
    self.imgUrl = imgUrl
    self.sex = sex
    self.brand = brand
    self.type = type
    self.category = category
    self.discount = discount

class JSONEncoder(json.JSONEncoder):
  def default(self, o):
    if isinstance(o, ObjectId):
      return str(o)
    return json.JSONEncoder.default(self, o)



@app.route('/allitems', methods=['GET'])
def api_all():
  items = list(mongo.db.items.find())
  return (JSONEncoder().encode(items))

@app.route('/shoes', methods=['GET'])
def api_shoes():
  items = list(mongo.db.items.find({'type': 'Shoes'}))
  return (JSONEncoder().encode(items))

@app.route('/authentication', methods=['POST'])
def api_auth():
  user =request.get_json()
  print(json.loads(request.data)['username'])
  users = list(mongo.db.auth.find())
  return (JSONEncoder().encode(users))



@app.route('/item/<int:id>', methods=['GET'])
def api_id(id):
  data = mongo.db.items.find_one({'uid': id})
  print (data)
  item = Item(data['uid'], data['short_description'], data['full_description'], data['xs'], data['s'], data['m'],
              data['l'], data['xl'], data['xxl'], data['price'], data['imgUrl'], data['sex'], data['brand'],
              data['type'], data['category'], data['discount'])

  return jsonify(
    uid=item.uid,
    short_description=item.short_description,
    full_description=item.full_description,
    xs=item.xs,
    s=item.s,
    m=item.m,
    l=item.l,
    xl=item.xl,
    xxl=item.xxl,
    price=item.price,
    imgUrl=item.imgUrl,
    sex=item.sex,
    brand=item.brand,
    type=item.type,
    category=item.category,
    discount=item.discount
  )

app.run()
