MONGO_URI = "mongodb://localhost:27017/api"


RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']

DOMAIN = {
  'items': {
    'schema': {
          'uid' : {
            'type': 'number',
            'minlength': 1,
            'required': True
          },
          'short_description' : {
            'type': 'string',
            'required': True
          },
          'full_description': {
            'type': 'string',
            'required': True
          },
          'xs': {
            'type': 'number',
            'minlength': 1,
            'required': True
          },
          's': {
            'type': 'number',
            'minlength': 1,
            'required': True
          },
          'm': {
            'type': 'number',
            'minlength': 1,
            'required': True
          },
          'l': {
            'type': 'number',
            'minlength': 1,
            'required': True
          },
          'xl': {
            'type': 'number',
            'minlength': 1,
            'required': True
          },
          'xxl': {
            'type': 'number',
            'minlength': 1,
            'required': True
          },
          'price' : {
            'type' : 'number',
            'minlength': 3,
            'required': True
          },
          'imgUrl': {
            'type': 'string',
            'minlength': 3,
            'required': True
          }
    }
  }

}
